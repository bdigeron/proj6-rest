# Project 6: Brevet time calculator service

Author:Bryce Di Geronimo
Email:bdigeron@uoregon.edu
Repo:https://bitbucket.org/bdigeron/proj6-rest
Description: Building on top of project 5 with the brevets calculator, this project implements an API to return the open and close times in JSON and csv format and a consumer program that exposes the API. 

There are three hosts, 5000 for the calculator, 5001 for the API, 5002 for the consumer program. 

To Access the flask brevets calculator, go to http://localhost:5000. 
To Access the API, to go http://localhost:5001 and use the services listed below.
    * "http://<host:port>/listAll" should return all open and close times in the database
    * "http://<host:port>/listOpenOnly" should return open times only
    * "http://<host:port>/listCloseOnly" should return close times only
    * "http://<host:port>/listAll/csv" should return all open and close times in CSV format
    * "http://<host:port>/listOpenOnly/csv" should return open times only in CSV format
    * "http://<host:port>/listCloseOnly/csv" should return close times only in CSV format
    * "http://<host:port>/listAll/json" should return all open and close times in JSON format
    * "http://<host:port>/listOpenOnly/json" should return open times only in JSON format
    * "http://<host:port>/listCloseOnly/json" should return close times only in JSON format
    * "http://<host:port>/listOpenOnly/csv?top=3" should return top 3 open times only (in ascending order) in CSV format 
    * "http://<host:port>/listOpenOnly/json?top=5" should return top 5 open times only (in ascending order) in JSON format
    * "http://<host:port>/listCloseOnly/csv?top=6" should return top 5 close times only (in ascending order) in CSV format
    * "http://<host:port>/listCloseOnly/json?top=4" should return top 4 close times only (in ascending order) in JSON format

To access the consumer program, go to http://localhost:5002 where further instructions are listed. To access the times without entering a top value, go to http://localhost:5002/listall.php and to enter a top value to get top times, go to 
http://localhost:5002/listtoptimes.php and use top?=x where x is the top time value you want. An example is 
http://localhost:5002/listtoptimes.php?top=3  Please note that if the value of top is negative or above the total number of times or none is given, the total number of times will be returned.

Special Cases:
Entering a negative value for a top value returns all values. 

Not accounted for:
If the checkpoints are not entered in order, the top times will not be returned properly. 




