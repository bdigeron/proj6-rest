<html>
    <head>
        <title>CIS 322 REST-api</title>
        <link rel="stylesheet" type="text/css" href="style.css">
    </head>

    <body>
        <h1>listAll and listAll/json</h1>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service/listAll/json');
            $obj = json_decode($json);
              $times = $obj->result;
            foreach ($times as $t) {
                echo "<li>Open: $t->open</li>";
                echo "<li>Close: $t->close</li>";
            }
            ?>
        </ul>

        <h1>listOpenOnly and listOpenOnly/json</h1>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service/listOpenOnly/json');
            $obj = json_decode($json);
              $times = $obj->result;
            foreach ($times as $t) {
                echo "<li>Open: $t->open</li>";
            }
            ?>
        </ul>

        <h1>listCloseOnly and listCloseOnly/json</h1>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service/listCloseOnly/json');
            $obj = json_decode($json);
              $times = $obj->result;
            foreach ($times as $t) {
                echo "<li>Close: $t->close</li>";
            }
            ?>
        </ul>

        <h1>listOpenOnly/csv</h1>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service/listOpenOnly/json');
            $obj = json_decode($json);
              $times = $obj->result;
            echo "<li>open</li>";
            foreach ($times as $t) {
                echo "<li>$t->open</li>";
            }
            ?>
        </ul>

        <h1>listCloseOnly/csv</h1>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service/listCloseOnly/json');
            $obj = json_decode($json);
              $times = $obj->result;
            echo "<li>close</li>";
            foreach ($times as $t) {
                echo "<li>$t->close</li>";
            }
            ?>
        </ul>

        <h1>listAll/csv</h1>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service/listAll/json');
            $obj = json_decode($json);
              $times = $obj->result;
            echo "<li>open, close </li>";
            foreach ($times as $t) {
                echo "<li>$t->open, $t->close</li>";
            }
            ?>
        </ul>

        
    </body>
</html>
